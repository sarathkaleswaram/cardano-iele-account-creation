pragma solidity ^0.4.0;


contract AccountCreation {
    struct User {
        address accountId;
        string userId;
        uint256 timestamp;
    }
    int private count = 0;

    mapping(address => User) users;

    function createUser(address accountId, string userId) public {
        count += 1;
        users[accountId].accountId = accountId;
        users[accountId].userId = userId;
        users[accountId].timestamp = block.timestamp;
    }

    function getCount() public constant returns (int) {
        return count;
    }

    function getUser(address accountId)
        public
        constant
        returns (address, string, uint256)
    {
        return (
            users[accountId].accountId,
            users[accountId].userId,
            users[accountId].timestamp
        );
    }
}
