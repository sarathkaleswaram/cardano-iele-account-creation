var express = require("express");
var app = express();
var nconf = require('nconf');
var log4js = require('log4js');
var morgan = require('morgan');
const Mallet = require('@iohk/mallet');
const fs = require('fs-extra');
const cors = require('cors');
const bodyParser = require('body-parser');
const compression = require('compression');
const sleep = require('sleep');


// Logger
log4js.configure({
  appenders: {
    out: { type: 'stdout', layout: { type: 'pattern', pattern: '%[%p %m%]' } },
    log: { type: 'file', filename: 'logs/log_' + new Date().toISOString() + '.log', layout: { type: 'pattern', pattern: '%d %r %p %m' } }
  },
  categories: {
    default: { appenders: ['out', 'log'], level: 'debug' }
  }
});
var logger = log4js.getLogger();
logger.level = 'debug';


// Mallet & Config
nconf.use('file', { file: './config.json' });
nconf.load();

const datadir = '/tmp/mallet-test-datadir';
fs.removeSync(datadir);

const mallet = new Mallet('iele', datadir);

var PRIVATEKEY = nconf.get('privateKey');
var ACCOUNT = nconf.get('account');
var CONTRACTADDRESS = nconf.get('contractAddress');

mallet.importPrivateKey(PRIVATEKEY, '');
mallet.selectAccount(ACCOUNT);

function awaitReceipt(hash) {
  let h = hash || mallet.lastTransaction().hash;
  let rec = mallet.getReceipt(h);
  if (rec) {
    return rec;
  } else {
    logger.debug("Awaiting receipt for " + h);
    sleep.sleep(3);
    return awaitReceipt(h);
  }
}

logger.info('Account = ' + ACCOUNT)
let balance = mallet.getBalance(ACCOUNT);
logger.info('Account balance = ' + balance)

if (CONTRACTADDRESS != undefined) {
  logger.info('Existing deployed contractAddress found in config.json - Contract Address:', CONTRACTADDRESS);
} else {
  logger.warn('No deployed contractAddress found in config.json');
  const contract = mallet.iele.compile('contract/AccountCreation.sol');
  mallet.iele.deployContract({ gas: 1000000, code: contract.bytecode }, '');
  const creationReceipt = awaitReceipt();
  CONTRACTADDRESS = creationReceipt.contractAddress;
  logger.info('New contract successfully compiled and deployed - Contract Address:', CONTRACTADDRESS);
  // Store the deployed contract address in local config file
  nconf.set('contractAddress', CONTRACTADDRESS);
  nconf.save(function (err) {
    if (err) {
      console.error(err.message);
      return;
    }
    logger.info('contract ' + CONTRACTADDRESS + ' was stored in config.json');
  });
}


// Express
app.use(cors());
app.use(morgan('dev'));
app.use(compression());
app.use(bodyParser.json());

app.get("/", (req, res, next) => {
  res.send("Cardano API Running!!!");
});

app.get("/count", (req, res, next) => {
  try {
    logger.debug("Get User Count");
    mallet.iele.callContract({
      to: CONTRACTADDRESS,
      gas: 1000000,
      func: 'getCount()',
      args: []
    }, '');
    const callReceipt = awaitReceipt();
    logger.debug(callReceipt);
    let sendResponse = {};
    sendResponse.count = mallet.iele.dec(callReceipt.returnData[0], 'int');
    logger.debug("Sending Response: ", sendResponse);
    res.send(sendResponse);
  } catch (error) {
    logger.error(error);
    res.send("Request Failed.");
  }
});

app.get("/query/:accountId", (req, res, next) => {
  try {
    logger.debug("Get User Details AccounId:", req.params.accountId);
    if (!req.params.accountId) {
      throw "Empty Address";
      return;
    }
    mallet.iele.callContract({
      to: CONTRACTADDRESS,
      gas: 1000000,
      func: 'getUser(address)',
      args: [mallet.iele.enc(req.params.accountId, 'address')]
    }, '');
    const callReceipt = awaitReceipt();
    logger.debug(callReceipt);
    let sendResponse = {};
    sendResponse.accountId = mallet.iele.dec(callReceipt.returnData[0], 'address');
    sendResponse.userId = mallet.iele.dec(callReceipt.returnData[1], 'string');
    sendResponse.timestamp = new Date(mallet.iele.dec(callReceipt.returnData[2], 'uint256') * 1000);
    logger.debug("Sending Response: ", sendResponse);
    res.send(sendResponse);
  } catch (error) {
    logger.error(error);
    res.send("Request Failed.");
  }
});

app.post('/create', function (req, res) {
  try {
    logger.debug("Create User Body:", req.body);
    if (!req.body) {
      throw "Empty Body";
      return;
    }
    if (!req.body.accountId || !req.body.userId) {
      throw "Missing required fields";
      return;
    }
    mallet.iele.callContract({
      to: CONTRACTADDRESS,
      gas: 1000000,
      func: 'createUser(address, string)',
      args: [mallet.iele.enc(req.body.accountId, 'address'), mallet.iele.enc(req.body.userId, 'string')]
    }, '');
    const callReceipt = awaitReceipt();
    logger.debug(callReceipt);
    let sendResponse = {};
    sendResponse.status = "Success";
    sendResponse.transactionHash = callReceipt.transactionHash;
    sendResponse.blockNumber = callReceipt.blockNumber;
    logger.debug("Sending Response: ", sendResponse);
    res.send(sendResponse);
  } catch (error) {
    logger.error(error);
    res.send("Request Failed.");
  }
});

app.listen(3000, () => {
  logger.info("Server running on port 3000");
});
